@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
             
                <div class="card-body">
                    <div class="container">
                        <center>
                          <h1>Proposals</h1>
                  
                          <h3>List of Proposals</h3>
                          <div class="row">
                              <div class="col-6">
                                  <form action="/">
                                      <input type="text" name="search" class="form-control">
                                      <button type="submit" class="btn btn-primary">Search</button>
                                  </form>
                              </div>
                              <div class="col-6">
                                  <a href="/add_proposal">
                                      <button class="btn btn-primary">Add Proposal</button>
                                  </a>
                              </div>
                          </div>
                          <table class="table">
                   
                              <tr>
                                  <td>Id</td>
                                  <td>Company Name</td>
                                  <td>Mobile Number</td>
                                  <td>Contact Name</td>
                                  <td>Email</td>
                                  <td>Image</td>
                                  <td>Due date</td>
                                  <td>action</td>
                              </tr>
                              @foreach ($list as $item)
                              <tr>
                                  <td>{{$item->id}}</td>
                                  <td>{{$item->company_name}}</td>
                                  <td>{{$item->contact_number}}</td>
                                  <td>{{$item->contact_name}}</td>
                                  <td>{{$item->email}}</td>
                                  <td><img src="/{{$item->attachment}}" height="50px"></td>
                                  <td>{{$item->due_date}}</td>
                                  
                                  <td><a href="/delete/{{$item->id}}"><button class="btn btn-danger">Delete</button></a></td>
                              </tr>
                              @endforeach
                          </table>
                      <center>
                      </div>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





