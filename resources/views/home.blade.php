<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>
    <div class="container">
      <center>
        <h1>Wallet</h1>

        <h3>List of wallets</h3>
        <a href="/add_wallet"><button class="btn btn-primary">Add Wallet</button></a>
        <table class="table">
            <tr>
                <td>Wallet id</td>
                <td>name</td>
                <td>Mobile Number</td>
                <td>Currency</td>
                <td>Symbol</td>
                <td>balance</td>
                <td>Conversion as per $1</td>
                <td>action</td>
            </tr>
            @foreach ($wallets as $wallet)
            <tr>
                <td>{{$wallet->id}}</td>
                <td>{{$wallet->name}}</td>
                <td>{{$wallet->mobile}}</td>
                <td>{{$wallet->currencies->name}}</td>
                <td>{{$wallet->currencies->symbol}}</td>
                <td>0</td>
                <td><button class="btn btn-warning">Add Money</button></td>
            </tr>
            @endforeach
        </table>
    <center>
    </div>
</body>
</html>