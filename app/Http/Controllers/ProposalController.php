<?php

namespace App\Http\Controllers;

use App\Models\Proposal;
use Illuminate\Http\Request;

class ProposalController extends Controller
{
   public function index()
   {
    if(isset($_GET['search'])){
        $query=$_GET['search'];
        $list=Proposal::where('company_name','LIKE','%'.$query.'%')->get();
        return view('deliveinsight.list',['list'=>$list]);
    }else{
        $list=Proposal::get();
          return view('deliveinsight.list',['list'=>$list]);
    }
   }

   public function add()
   {
      return view('deliveinsight.add');
   }

   public function edit()
   {
      return view('deliveinsight.edit');
   }

   public function delete(Request $request,$id)
   {
    // dd($id);
      Proposal::where('id',$id)->delete();
      return redirect('/');
   }

   public function store_proposal(Request $request)
   {

    $request->validate([
        'name'=>'required',
        'email'=>'required',
        'contact_name'=>'required',
        'mobile'=>'required',
        'due_date'=>'required',
        'file'=>'required',
    ]);
      
    $data=$request->all();
    $file=$request->file('file');
    $filename=time().$file->getClientOriginalName();
    $destination='attachment';
    $upload_file=$file->move($destination,$filename);
    
    $create=Proposal::create([
        'company_name'=>$data['name'],
        'contact_number'=>$data['mobile'],
        'email'=>$data['email'],
        'contact_name'=>$data['contact_name'],
        'due_date'=>$data['due_date'],
        'attachment'=>$destination."/".$filename
    ]);

    return redirect('/');

      
   }
}
